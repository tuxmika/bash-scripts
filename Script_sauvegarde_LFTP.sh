#!/bin/bash

# Script de sauvegarde LFTP distant > local
# Licence CC BY-NC-SA 4.0 ( https://creativecommons.org/licenses/by-nc-sa/4.0/ )
# Auteur : Mickaël BONNARD ( https://www.mickaelbonnard.fr )
# Pré-requis : lftp ( http://lftp.yar.ru )

userftp=user
passftp=password
hostftp=ftp.user.fr
jour=$(date +%d-%m-%Y)
logs=/home/user/sauvegardes/logs
log=/home/user/sauvegardes/logs/sauvegarde_wiki_`date +%d-%m-%Y`
local="/home/user/sauvegardes/"
distant="wiki.user.fr"
rotation=11 

# Si le dossier des logs n'existe pas, celui-ci sera créé.

test -d $logs || mkdir -p $logs

# Tout ce qui est OK et KO sera écrit dans $log.

exec > $log 2>&1

echo "-------------------------------------------------------------" 
echo  "Sauvegarde de $distant du $(date +%d-%m-%Y)" 
echo "-------------------------------------------------------------" 
echo  "Début de la sauvegarde à `date +%T`" 
echo "-------------------------------------------------------------" 

# On se place dans le répertoire local

cd $local

# manuel lftp : https://lftp.yar.ru/lftp-man.html
# userftp, passftp, hostftp : identifiants FTP.
# -e de lftp avec des arguments entre guillemets : spécifie la commande ou les commandes à éxécuter
# mirror : Commande permettant d'effectuer une synchronisation entre un répertoire local et un répertoire distant.
# ftp:ssl-force true : On négocie une connexion SSL avec le serveur FTP.
# ftp:ssl-protect-data true : On demande une connexion SSL pour les transferts de données.
# ftp:ssl:verify-certificate no :  On désactive la vérification des certificats.
# distant : Votre dossier distant.
# local : Votre chemin local.
# quit : coupe la connexion après le transfert.

# On lance la sauvegarde et la compression

lftp "${userftp}:${passftp}@${hostftp}" -e "set ftp:ssl-force true; set ftp:ssl-protect-data true; set ssl:verify-certificate no; mirror $distant wiki_$jour; quit" \
 && tar -zcvf wiki_$jour.tar.gz wiki_$jour 

# On effectue la rotation

echo "-------------------------------------------------------------"
echo  "Liste des sauvegardes avant rotation" 
echo "-------------------------------------------------------------"  

ls -lrth *.tar.gz | awk {'print $7" "$6" "$9" "$5'} 

nombre_sauvegardes=$(ls -1 $local/*.tar.gz | wc -l)

ancien=$(ls -1rt $local/*.tar.gz | head -1)

if [ $nombre_sauvegardes -eq $rotation ]

then

rm -rf $ancien

fi

echo "-------------------------------------------------------------" 
echo  "Liste des sauvegardes après rotation" 
echo "-------------------------------------------------------------" 

ls -lrth *.tar.gz | awk {'print $7" "$6" "$9" "$5'} 

echo "-------------------------------------------------------------" 
echo "Fin de la sauvegarde de $distant le $(date +%d-%m-%Y) à `date +%T`" 
echo "-------------------------------------------------------------" 

# On envoie un mail récapitulatif

cat $log | mutt -s "Sauvegarde de $distant du $jour" mail@mail.fr

# On supprime le dossier et le log

rm -rf wiki_$jour

rm -rf $log

