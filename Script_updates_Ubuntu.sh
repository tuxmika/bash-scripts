#!/bin/bash
# Script mises à jour Ubuntu
# Le script va lister le nombre et le nom des paquets qui seront mis à jour.
# Il va ensuite les installer et pour terminer envoyer un mail récapitulatif.
# Une fois le traitement terminé ou si aucune mise à jour n'est disponible,un shutdown sera effectué. 
# Auteur : Mickaël BONNARD ( https://www.mickaelbonnard.fr )

# mettre dans /etc/sudoers : nom_utilisateur ALL=NOPASSWD: /usr/bin/updates

jour=`date +%d-%m-%Y`
heure=`date +%H:%M:%S`
dossier_logs="/home/user/.logs"
log="updates_`date +%d-%m-%Y`.log"
dpkg=/var/log/dpkg.log
destinataire=mail@mail.fr

[ -d $dossier_logs ] || mkdir -p $dossier_logs

exec > $dossier_logs/$log 2>&1

> $dpkg

export DEBIAN_FRONTEND=noninteractive

notify-send -t 5000 "Vérification des mises à jour..." "Veuillez patienter quelques instants..."

echo "-------------------------------------------------------------------------------------------------" 

echo -e "\tMises à jour du $(date +%d" "%B" "%Y)" sur $HOSTNAME

echo "-------------------------------------------------------------------------------------------------" 

echo -e "\tDébut du traitement le $(date +%d-%B-%Y) à $(date +%H:%M:%S)"

echo "-------------------------------------------------------------------------------------------------" 

echo -e "\tEtape 1 : Mise à jour des dépôts"

echo "-------------------------------------------------------------------------------------------------"

apt-get update 

nombre=$(apt-get -s full-upgrade | grep "Inst" | awk '{print $2}' | wc -l )

if [ $nombre -ne 0 ]

then 

notify-send -t 5000 "$nombre mise(s) à jour disponible(s)"

echo "-------------------------------------------------------------------------------------------------"

echo -e "\tEtape 2 : Liste des mises à jours disponibles" 

echo "-------------------------------------------------------------------------------------------------" 

liste_correctifs=$(apt-get -s full-upgrade | grep "Inst" | awk '{print $2}')

for paquet in $liste_correctifs ;do

versions=$(apt-cache policy $paquet | head -3)

echo -e "$versions"

echo "-------------------------------------------------------------------------------------------------" 

done

echo -e "\tNombre total de mises à jour disponibles : $nombre"

echo "-------------------------------------------------------------------------------------------------" 

echo -e "\tEtape 3 : Installation des mises à jour"

echo "-------------------------------------------------------------------------------------------------" 

# La commande full-upgrade va remplir la même fonction qu'upgrade en permettant également de supprimer des paquets installés si cela est nécessaire afin de résoudre un conflit entre des paquets.

# https://man7.org/linux/man-pages/man1/dpkg.1.html

apt-get full-upgrade -y -o Dpkg::Options::="--force-confnew" -o Dpkg::Options::="--force-confdef" > /dev/null

nombre_ok=$(grep "status installed" $dpkg | awk {'print $5'} | awk -F : {'print $1'} | wc -l)

grep "status installed" $dpkg | awk {'print $5" "$6" "$7'}

echo "-------------------------------------------------------------------------------------------------" 

echo -e "\tNombre total de paquets mis à jour ou installés : $nombre_ok"

echo "-------------------------------------------------------------------------------------------------" 

echo -e "\tEtape 4 : Nettoyage"

echo "-------------------------------------------------------------------------------------------------" 

apt-get clean && apt-get autoclean && apt-get autoremove -y

else

notify-send -t 5000 "Aucune mise à jour disponible"

rm -rf $dossier_logs/$log

sleep 20s && shutdown -h now

fi

echo "-------------------------------------------------------------------------------------------------" 

echo -e "\tFin du traitement le $(date +%d-%B-%Y) à $(date +%H:%M:%S)"

echo "-------------------------------------------------------------------------------------------------" 

s-nail -s "Mises à jour du $(date +%d" "%B" "%Y) sur $HOSTNAME" $destinataire < $dossier_logs/$log

export DEBIAN_FRONTEND=dialog

find $dossier_logs -type f -mtime +15 -exec rm -rf {} \;

sleep 20s && shutdown -h now
