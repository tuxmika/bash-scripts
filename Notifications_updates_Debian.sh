#!/bin/bash
# Script de notification de mises à jour Debian 12
# Auteur : Mickaël BONNARD ( https://www.mickaelbonnard.fr )
# Licence CC BY-NC-SA 4.0 ( https://creativecommons.org/licenses/by-nc-sa/4.0/ )
# Prérequis : mutt

# Variables
ip=$(hostname -I)
sujet="Mises à jour disponibles sur $HOSTNAME"
destinataire="mail@mail.fr"
log="/var/log/updates_`date +%d-%m-%Y`"

exec > $log 2>&1

liste_maj=$(apt-get -s dist-upgrade | grep "Inst" | awk '{print $2}')

nombre_maj=$(apt-get -s dist-upgrade | grep "Inst" | awk '{print $2}'| wc -l)

apt-get -qq update

maj_debian(){
echo -e "-------------------------------------------------------------------------------------------------\n"
echo -e "$nombre_maj mises à jour disponibles sur $HOSTNAME ( $ip)\n"
for paquet in $liste_maj ;do
versions=$(apt-cache policy $paquet | head -3)
echo -e "-------------------------------------------------------------------------------------------------\n"
echo -e "$versions\n"
echo -e "informations sur le paquet:
https://packages.debian.org/fr/bookworm/${paquet}\n"
done
echo -e "-------------------------------------------------------------------------------------------------\n"
}

maj_debian

cat $log | mutt -s "$sujet" "$destinataire"