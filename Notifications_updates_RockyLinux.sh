#!/bin/bash
# Script de notification de mises à jour Rocky Linux
# Auteur : Mickaël BONNARD ( https://www.mickaelbonnard.fr )
# Licence CC BY-NC-SA 4.0 ( https://creativecommons.org/licenses/by-nc-sa/4.0/ )
# Prérequis : mutt

# Variables
ip=$(hostname -I)
sujet="Mises à jour disponibles sur $HOSTNAME"
destinataire="mail@mail.fr"
log="/var/log/updates_`date +%d-%m-%Y`"

exec > $log 2>&1

dnf check-update > /dev/null

liste_maj=$(dnf -q check-update | tail -n+2 | awk {'print $1'})

nombre_maj=$(dnf -q check-update | tail -n+2 | wc -l)

maj_rockylinux(){
echo -e "------------------------------------------------------------------------------------------------------\n"
echo -e "$nombre_maj mises à jour disponibles sur $HOSTNAME ( $ip)\n"
echo -e "------------------------------------------------------------------------------------------------------\n"
for paquet in $liste_maj ;do
actuelle=$(dnf list installed $paquet | awk {'print $1,$2,$3'} | tail -n+2)
maj=$(dnf -q check-update | tail -n+2 | awk {'print $1,$2,$3'} | grep ^$paquet)
echo -e "$paquet :\n\nInstallé : $actuelle\nCandidat : $maj\n"
echo -e "------------------------------------------------------------------------------------------------------\n"
done
}

maj_rockylinux

cat $log | mutt -s "$sujet" "$destinataire"

